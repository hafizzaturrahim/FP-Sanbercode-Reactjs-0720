import React, { useContext, useState } from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import TextField from '@material-ui/core/TextField';

import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

import Typography from '@material-ui/core/Typography';
import { Link } from "react-router-dom";
import { MovieContext } from './MovieContext';

const useStyles = makeStyles((theme) => ({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
  root: {
    '& .MuiTextField-root': {
      marginBottom: theme.spacing(2),
      marginRight: theme.spacing(2),
    },
    width: 300
  },

}));

export default function MovieFilter() {
  const classes = useStyles();
  const theme = useTheme();
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });
  const [open, setOpen] = React.useState(false);
  const handleDrawerClose = () => {
    setOpen(false);
  };

  const toggleDrawer = (anchor, open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const [listMovie, setlistMovie] = useContext(MovieContext)
  const [inputFilter, setInputFilter] = useState({})
  const [tempMovie, setTempMovie] = useState(null)

  if (listMovie !== null && tempMovie === null) {
    setTempMovie(listMovie)
  }

  const handleChange = (event) => {
    let typeOfInput = event.target.name

    switch (typeOfInput) {
      case "title":
        {
          setInputFilter({ ...inputFilter, title: event.target.value });
          break
        }
      case "genre":
        {
          setInputFilter({ ...inputFilter, genre: event.target.value });
          break
        }
      case "yearMin":
        {
          setInputFilter({ ...inputFilter, yearMin: event.target.value });
          break
        }
      case "yearMax":
        {
          setInputFilter({ ...inputFilter, yearMax: event.target.value });
          break
        }
      case "ratingMin":
        {
          let rating = parseInt(event.target.value)
          if (rating < 1) {
            rating = 1
          } else if (rating > 10) {
            rating = 9
          }
          setInputFilter({ ...inputFilter, rating });
          break
        }
      case "ratingMax":
        {
          let rating = parseInt(event.target.value)
          if (rating < inputFilter.ratingMin) {
            rating = inputFilter.ratingMin
          }
          if (rating < 1) {
            rating = 1
          } else if (rating > 10) {
            rating = 9
          }
          setInputFilter({ ...inputFilter, rating });
          break
        }
      default:
        { break; }
    }
  }

  const handleFilter = (event) => {
    // menahan submit
    event.preventDefault()

    let title = event.target.title.value
    let genre = event.target.genre.value
    let yearMin = event.target.yearMin.value
    let yearMax = event.target.yearMax.value
    let ratingMin = event.target.ratingMin.value
    let ratingMax = event.target.ratingMax.value

    let filteredMovie = null
    if(title.replace(/\s/g, '') !== ""){
      filteredMovie = listMovie.filter(el =>
        el.title.toLowerCase().includes(title.toLowerCase()))
    }

    if(genre.replace(/\s/g, '') !== ""){
      filteredMovie = listMovie.filter(el =>
        el.genre.toLowerCase().includes(genre.toLowerCase()))
    }

    if(yearMin.replace(/\s/g, '') !== ""){
      filteredMovie = listMovie.filter(el =>
        el.year >= yearMin)
    }

    if(yearMax.replace(/\s/g, '') !== ""){
      filteredMovie = listMovie.filter(el =>
        el.year <= yearMax)
    }

    if(ratingMin.replace(/\s/g, '') !== ""){
      filteredMovie = listMovie.filter(el =>
        el.rating >= ratingMin)
    }

    if(ratingMax.replace(/\s/g, '') !== ""){
      filteredMovie = listMovie.filter(el =>
        el.rating <= ratingMax)
    }
    

    console.log("filter movie")
    console.log(filteredMovie)
    if(filteredMovie !== null){
      setlistMovie([...filteredMovie])

    }
  }

  const resetFilter = () => {
    console.log("temp")
    console.log(tempMovie)
    setlistMovie(tempMovie)
    setInputFilter({})
    console.log("inputMovie")
    console.log(tempMovie)
  }

  return (
    <div>
      {['left'].map((anchor) => (
        <React.Fragment key={anchor}>
          <center>

            <Typography className={classes.title} variant="h4" id="tableTitle" component="div">
              Daftar Film
                        </Typography>
          </center>
          <Button onClick={toggleDrawer(anchor, true)} color="primary" variant="contained">Lakukan Filter</Button>
          <Button color="primary" variant="contained" style={{ marginBottom: "16px", float: "right" }}><Link to="/movie/create" style={{ color: "white" }}>Buat Review Film</Link></Button>

          <Drawer anchor="left" open={state["left"]} onClose={toggleDrawer("left", false)}>
            {/* {list(anchor)} */}
            <div className={classes.drawerHeader}>
              <IconButton onClick={handleDrawerClose}>
                {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
              </IconButton>
            </div>
            <Divider />
            {inputFilter !== null &&
              <div style={{ padding: "16px" }}>
                <h2>Filter</h2>
                <form className={classes.root} autoComplete="off" onSubmit={handleFilter}>
                  <TextField fullWidth size="small" InputLabelProps={{ shrink: true }} id="outlined-basic" label="Judul Film" variant="outlined" name="title" onChange={handleChange} value={inputFilter.title} />
                  <TextField fullWidth size="small" InputLabelProps={{ shrink: true }} id="outlined-basic" label="Genre" variant="outlined" name="genre" onChange={handleChange} value={inputFilter.genre} />
                  <Divider />
                  <h3>Tahun</h3>
                  <TextField InputLabelProps={{ shrink: true }} id="outlined-basic" label="Awal" variant="outlined" name="yearMin" style={{ width: 100 }} type="number" onChange={handleChange} value={inputFilter.yearMin} />
                  <h4 style={{ display: "inline" }}><strong>s/d</strong></h4>
                  <TextField InputLabelProps={{ shrink: true }} id="outlined-basic" label="Akhir" variant="outlined" name="yearMax" style={{ width: 100, marginLeft: theme.spacing(2) }} type="number" onChange={handleChange} value={inputFilter.yearMax} />
                  <Divider />
                  <h3>Rating</h3>
                  <TextField InputLabelProps={{ shrink: true }} id="outlined-basic" label="Minimum" variant="outlined" name="ratingMin" style={{ width: 100 }} type="number" onChange={handleChange} value={inputFilter.ratingMin} />
                  <h4 style={{ display: "inline" }}><strong>s/d</strong></h4>
                  <TextField InputLabelProps={{ shrink: true }} id="outlined-basic" label="Maksimum" variant="outlined" name="ratingMax" style={{ width: 100, marginLeft: theme.spacing(2) }} type="number" onChange={handleChange} value={inputFilter.ratingMax} />
                  <Divider />

                  <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    fullWidth
                    style={{ marginTop: theme.spacing(2) }}
                  >
                    Filter
                </Button>
                <Button
                    variant="contained"
                    color="secondary"
                    fullWidth
                    style={{ marginTop: theme.spacing(1) }}
                    onClick = {resetFilter}
                  >
                    Reset
                </Button>
                </form>
              </div>
            }
          </Drawer>
        </React.Fragment>
      ))}
    </div>
  );
}