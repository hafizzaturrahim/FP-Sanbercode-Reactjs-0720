import React, { useContext, useState } from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

import Typography from '@material-ui/core/Typography';
import { Link } from "react-router-dom";
import { GameContext } from './GameContext';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

const useStyles = makeStyles((theme) => ({
    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },
    root: {
        '& .MuiTextField-root': {
            marginBottom: theme.spacing(2),
            marginRight: theme.spacing(2),
        },
        width: 300
    },

}));

export default function GameFilter() {
    const classes = useStyles();
    const theme = useTheme();
    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
    });
    const [open, setOpen] = React.useState(false);
    const handleDrawerClose = () => {
        setOpen(false);
    };

    const toggleDrawer = (anchor, open) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setState({ ...state, [anchor]: open });
    };

    const [listGame, setListGame] = useContext(GameContext)
    const [inputFilter, setInputFilter] = useState({})
    const [tempGame, setTempGame] = useState(null)

    if (listGame !== null && tempGame === null) {
        setTempGame(listGame)
    }

    const handleChange = (event) => {
        let typeOfInput = event.target.name

        switch (typeOfInput) {
            case "nameGame":
                {
                    setInputFilter({ ...inputFilter, name: event.target.value });
                    break
                }
            case "genre":
                {
                    setInputFilter({ ...inputFilter, genre: event.target.value });
                    break
                }
            case "yearMin":
                {
                    setInputFilter({ ...inputFilter, yearMin: event.target.value });
                    break
                }
            case "yearMax":
                {
                    setInputFilter({ ...inputFilter, yearMax: event.target.value });
                    break
                }
            case "singlePlayer":
                {
                    setInputFilter({ ...inputFilter, singlePlayer: event.target.value });
                    break
                }
            case "multiplayer":
                {
                    setInputFilter({ ...inputFilter, multiplayer: event.target.value });
                    break
                }
            default:
                { break; }
        }
    }

    const handleFilter = (event) => {
        // menahan submit
        event.preventDefault()

        let name = event.target.nameGame.value
        let genre = event.target.genre.value
        let yearMin = event.target.yearMin.value
        let yearMax = event.target.yearMax.value
        let singlePlayer = parseInt(event.target.singlePlayer.value)
        let multiplayer = parseInt(event.target.multiplayer.value)

        console.log(singlePlayer)

        let filteredGame = null
        if (name.replace(/\s/g, '') !== "") {
            filteredGame = listGame.filter(el =>
                el.name.toLowerCase().includes(name.toLowerCase()))
        }

        if (genre.replace(/\s/g, '') !== "") {
            filteredGame = listGame.filter(el =>
                el.genre.toLowerCase().includes(genre.toLowerCase()))
        }

        if (yearMin.replace(/\s/g, '') !== "") {
            filteredGame = listGame.filter(el =>
                el.release >= yearMin)
        }

        if (yearMax.replace(/\s/g, '') !== "") {
            filteredGame = listGame.filter(el =>
                el.release <= yearMax)
        }

        if (singlePlayer !== 2) {
            filteredGame = listGame.filter(el =>
                el.singlePlayer == singlePlayer)
        }

        if (multiplayer !== 2) {
            filteredGame = listGame.filter(el =>
                el.multiplayer == multiplayer)
        }


        console.log("filter game")
        console.log(filteredGame)
        if (filteredGame !== null) {
            setListGame([...filteredGame])

        }
    }

    const resetFilter = () => {
        console.log("temp")
        console.log(tempGame)
        setListGame(tempGame)
        setInputFilter({})
        console.log("input game")
        console.log(tempGame)
    }

    return (
        <div>
            {['left'].map((anchor) => (
                <React.Fragment key={anchor}>
                    <center>

                        <Typography className={classes.title} variant="h4" id="tableTitle" component="div">
                            Daftar Game
                        </Typography>
                    </center>
                    <Button onClick={toggleDrawer(anchor, true)} color="primary" variant="contained">Lakukan Filter</Button>
                    <Button color="primary" variant="contained" style={{ marginBottom: "16px", float: "right" }}><Link to="/game/create" style={{ color: "white" }}>Buat Review Game</Link></Button>

                    <Drawer anchor="left" open={state["left"]} onClose={toggleDrawer("left", false)}>
                        {/* {list(anchor)} */}
                        <div className={classes.drawerHeader}>
                            <IconButton onClick={handleDrawerClose}>
                                {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                            </IconButton>
                        </div>
                        <Divider />
                        {inputFilter !== null &&
                            <div style={{ padding: "16px" }}>
                                <h2>Filter</h2>
                                <form className={classes.root} autoComplete="off" onSubmit={handleFilter}>
                                    <TextField fullWidth size="small" InputLabelProps={{ shrink: true }} id="outlined-basic" label="Judul Game" variant="outlined" name="nameGame" onChange={handleChange} value={inputFilter.title} />
                                    <TextField fullWidth size="small" InputLabelProps={{ shrink: true }} id="outlined-basic" label="Genre" variant="outlined" name="genre" onChange={handleChange} value={inputFilter.genre} />
                                    <Divider />

                                    <h3>Tahun</h3>
                                    <TextField InputLabelProps={{ shrink: true }} id="outlined-basic" label="Awal" variant="outlined" name="yearMin" style={{ width: 100 }} type="number" onChange={handleChange} value={inputFilter.yearMin} />
                                    <h4 style={{ display: "inline" }}><strong>s/d</strong></h4>
                                    <TextField InputLabelProps={{ shrink: true }} id="outlined-basic" label="Akhir" variant="outlined" name="yearMax" style={{ width: 100, marginLeft: theme.spacing(2) }} type="number" onChange={handleChange} value={inputFilter.yearMax} />
                                    <Divider />

                                    <FormControl component="fieldset" style={{ marginTop: theme.spacing(1) }}>
                                        <FormLabel component="legend">Single Player?</FormLabel>
                                        <RadioGroup aria-label="singlePlayer" name="singlePlayer" defaultValue="2" row>
                                            <FormControlLabel value="1" control={<Radio />} label="Ya" />
                                            <FormControlLabel value="0" control={<Radio />} label="Tidak" />
                                            <FormControlLabel value="2" control={<Radio />} label="No Filter" defaultChecked />
                                        </RadioGroup>
                                    </FormControl>

                                    <FormControl component="fieldset" style={{ marginTop: theme.spacing(1) }}>
                                        <FormLabel component="legend">Multi Player?</FormLabel>
                                        <RadioGroup aria-label="multiplayer" name="multiplayer" defaultValue="2" row>
                                            <FormControlLabel value="1" control={<Radio />} label="Ya" />
                                            <FormControlLabel value="0" control={<Radio />} label="Tidak" />
                                            <FormControlLabel value="2" control={<Radio />} label="No Filter" defaultChecked />
                                        </RadioGroup>
                                    </FormControl>
                                    <Divider />

                                    <Button
                                        type="submit"
                                        variant="contained"
                                        color="primary"
                                        fullWidth
                                        style={{ marginTop: theme.spacing(2) }}
                                    >
                                        Filter
                                    </Button>
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                        fullWidth
                                        style={{ marginTop: theme.spacing(1) }}
                                        onClick={resetFilter}
                                    >
                                        Reset
                                    </Button>
                                </form>
                            </div>
                        }
                    </Drawer>
                </React.Fragment>
            ))}
        </div>
    );
}