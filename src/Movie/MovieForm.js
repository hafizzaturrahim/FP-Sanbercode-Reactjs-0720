import React, { useContext } from "react";
import { MovieContext } from './MovieContext';
import { InputContext } from "./InputContext";
import axios from 'axios';
import { useHistory } from "react-router-dom";

const MovieForm = () => {
    const [listMovie, setlistMovie] = useContext(MovieContext)
    const [inputMovie, setinputMovie, selectedID, setselectedID] = useContext(InputContext)
    const history = useHistory();

    const handleChange = (event) => {
        let typeOfInput = event.target.name

        switch (typeOfInput) {
            case "title":
                {
                    setinputMovie({ ...inputMovie, title: event.target.value });
                    break
                }
            case "description":
                {
                    setinputMovie({ ...inputMovie, description: event.target.value });
                    break
                }
            case "year":
                {
                    setinputMovie({ ...inputMovie, year: event.target.value });
                    break
                }
            case "duration":
                {
                    setinputMovie({ ...inputMovie, duration: event.target.value });
                    break
                }
            case "genre":
                {
                    setinputMovie({ ...inputMovie, genre: event.target.value });
                    break
                }
            case "rating":
                {
                    let rating = parseInt(event.target.value)
                    if (rating < 1 || rating > 10) {
                        rating = 1
                    }
                    setinputMovie({ ...inputMovie, rating });
                    break
                }
            case "image_url":
                {
                    setinputMovie({ ...inputMovie, image_url: event.target.value });
                    break
                }
            case "review":
                {
                    setinputMovie({ ...inputMovie, review: event.target.value });
                    break
                }
            default:
                { break; }
        }
    }

    const handleSubmit = (event) => {
        // menahan submit
        event.preventDefault()

        console.log("tes handlesubmit")
        let title = inputMovie.title
        let description = inputMovie.description
        let year = inputMovie.year
        let duration = inputMovie.duration
        let genre = inputMovie.genre
        let rating = inputMovie.rating
        let image_url = inputMovie.image_url
        let review = inputMovie.review

        var today = new Date(),
            date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate() + ' ' + today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();

        let created_at = date
        let updated_at = date

        if (title.replace(/\s/g, '') !== "" && description.replace(/\s/g, '') !== "" && year.toString().replace(/\s/g, '') !== "" && duration.toString().replace(/\s/g, '') !== "" && genre.replace(/\s/g, '') !== "" && rating.toString().replace(/\s/g, '') !== "" && review.replace(/\s/g, '') !== "") {
            if (selectedID === -1) {
                axios.post(`https://backendexample.sanbersy.com/api/movies`, { title, description, year, duration, genre, rating, created_at })
                    .then(res => {
                        console.log("input sukses")
                        setlistMovie([...listMovie, { id: res.data.id, title, description, year, duration, genre, rating, image_url, review, created_at, updated_at }])
                    })
            } else if (selectedID !== -1) {
                axios.put(`https://backendexample.sanbersy.com/api/movies/${selectedID}`, { title, description, year, duration, genre, rating, image_url, updated_at })
                    .then(() => {
                        let movie = listMovie.find(el => el.id === selectedID)
                        movie.title = title
                        movie.description = description
                        movie.year = year
                        movie.duration = duration
                        movie.genre = genre
                        movie.rating = rating
                        movie.updated_at = updated_at
                        movie.image_url = image_url
                        movie.review = review
                        setlistMovie([...listMovie])
                    })
            }

            setselectedID(-1)
            setinputMovie({ title: "", description: "", year: "", duration: "", genre: "", rating: "" })
            history.push("/movie");
        }
    }


    return (
        <div>
            <h1>Form Film</h1>
            <div style={{ width: "80%", margin: "0 auto", display: "block" }}>
                <div style={{ border: "1px solid #aaa", padding: "20px" }}>
                    <form onSubmit={handleSubmit}>
                        <label>
                            Judul Film :
                        </label>
                        <input type="text" name="title" value={inputMovie.title} onChange={handleChange} />
                        <br />
                        <br />
                        <label>
                            Deskripsi:
                        </label>
                        <br />
                        <textarea name="description" value={inputMovie.description} onChange={handleChange} />
                        <br />
                        <br />
                        <label>
                            Tahun :
                        </label>
                        <input type="number" name="year" value={inputMovie.year} onChange={handleChange} />
                        <br />
                        <br />
                        <label>
                            Durasi (dalam menit) :
                        </label>
                        <input type="number" name="duration" value={inputMovie.duration} onChange={handleChange} />
                        <br />
                        <br />
                        <label>
                            Genre :
                        </label>
                        <input type="text" name="genre" value={inputMovie.genre} onChange={handleChange} />
                        <br />
                        <br />
                        <label>
                            Rating (1-10):
                        </label>
                        <input type="number" name="rating" value={inputMovie.rating} onChange={handleChange} placeholder="Isi antara 1- 10" />
                        <br />
                        <br />
                        <label>
                            Gambar (url) :
                        </label>
                        <input type="text" name="image_url" value={inputMovie.image_url} onChange={handleChange} />
                        <br />
                        <br />
                        <label>
                            Review:
                        </label>
                        <input type="text" name="review" value={inputMovie.review} onChange={handleChange} />
                        <br />
                        <br />
                        <button>submit</button>

                    </form>
                </div>
            </div>
        </div>
    )
}

export default MovieForm