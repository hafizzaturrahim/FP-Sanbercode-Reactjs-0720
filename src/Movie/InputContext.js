import React, { useState, createContext } from "react";

export const InputContext = createContext();
export const InputProvider = props => {
    const [inputMovie, setinputMovie] = useState({title: "", description: "", year: "", duration: "", genre : "", rating : ""})
    const [selectedID, setselectedID] = useState(-1)
    
    return (
        <InputContext.Provider value={[inputMovie, setinputMovie, selectedID, setselectedID]}>
            {props.children}
        </InputContext.Provider>
    );
};