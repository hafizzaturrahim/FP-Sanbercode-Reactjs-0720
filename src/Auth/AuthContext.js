import React, { useState, createContext, useEffect } from "react";

export const AuthContext = createContext();
export const AuthProvider = props => {
    const [authData, setauthData] = useState({
        isLogin : false,
        id:""
    })

    useEffect(()=>{
        if(localStorage.getItem('user')){
            setauthData({isLogin : true, id:localStorage.getItem('user')})
        }
    }, [])
    

    return (
        <AuthContext.Provider value={[authData, setauthData]}>
            {props.children}
        </AuthContext.Provider>
    );
}