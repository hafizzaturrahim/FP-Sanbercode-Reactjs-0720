import React, { useState, useEffect } from 'react';
import axios from 'axios';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(4),
        marginBottom: theme.spacing(4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function ChangePassword() {
    const classes = useStyles();
    const [userData, setUserData] = useState(null)

    const [notif, setnotif] = useState("")
    let id = localStorage.getItem("user")

    useEffect(() => {
        if (userData === null) {
            axios.get(`https://backendexample.sanbersy.com/api/users/${id}`)
                .then(res => {
                    console.log("get user by id")
                    setUserData({
                        id,
                        updated_at: res.data.updated_at,
                        username: res.data.username,
                        password: res.data.password
                    })
                    
                })
        }

    }, [userData])

    const handleSubmit = (event) => {
        event.preventDefault();
        let passwordOld = event.target.passwordOld.value
        let passwordNew = event.target.passwordNew.value
        console.log(userData.password)
        console.log(passwordOld)
        console.log(passwordNew)

        if (passwordOld === userData.password) {
            axios.put(`https://backendexample.sanbersy.com/api/users/${id}`, { password: passwordNew })
                .then(res => {
                    console.log("ganti pasword")
                    console.log(res)
                    setnotif("")
                    alert("Password berhasil diganti.")
                })
                .catch(res => {
                    console.log("ada error")
                    console.log(res)
                })
        } else {
            setnotif("Password yang dimasukkan salah")
        }



    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            {userData !== null && 
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Ubah Password
                </Typography>
                <Typography component="h3" variant="h5">
                    Username : {userData.username}
                </Typography>

                <form className={classes.form} noValidate onSubmit={handleSubmit}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="passwordOld"
                        label="Password Lama"
                        type="password"
                        id="passwordOld"

                    />
                    {notif !== null &&
                        <p style={{ color: "red" }}>{notif} </p>

                    }
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="passwordNew"
                        label="Password Baru"
                        type="password"
                        id="passwordNew"

                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Ganti Password
                    </Button>

                </form>
            </div>
            }
        </Container>
    );
}