import React, { useContext, useEffect } from "react"
import { MovieContext } from './MovieContext';
import { InputContext } from "./InputContext"
import axios from 'axios';
import { Link, useHistory } from "react-router-dom";

import PropTypes from 'prop-types';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
    },
    table: {
        minWidth: 750,
    },
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
}));

export default function MovieList() {
    const classes = useStyles();
    const [order, setOrder] = React.useState('asc');
    const [orderBy, setOrderBy] = React.useState('calories');
    const [selected, setSelected] = React.useState([]);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);

    const [listMovie, setlistMovie] = useContext(MovieContext)
    const [, setinputMovie, , setselectedID] = useContext(InputContext)
    const history = useHistory();

    const theme = useTheme();

    useEffect(() => {
        if (listMovie === null) {
            axios.get(`https://backendexample.sanbersy.com/api/movies`)
                .then(res => {
                    console.log("read data")
                    setlistMovie(res.data.map(el => {
                        return {
                            id: el.id,
                            title: el.title,
                            description: el.description,
                            year: el.year,
                            duration: el.duration,
                            genre: el.genre,
                            rating: el.rating
                        }
                    }))

                })
        }

    }, [listMovie])
    // dari atas
    const rows = listMovie;
    // console.log("rows")
    // console.log(listMovie)
    function descendingComparator(a, b, orderBy) {
        if (b[orderBy] < a[orderBy]) {
            return -1;
        }
        if (b[orderBy] > a[orderBy]) {
            return 1;
        }
        return 0;
    }

    function getComparator(order, orderBy) {
        return order === 'desc'
            ? (a, b) => descendingComparator(a, b, orderBy)
            : (a, b) => -descendingComparator(a, b, orderBy);
    }

    function stableSort(array, comparator) {
        const stabilizedThis = array.map((el, index) => [el, index]);
        stabilizedThis.sort((a, b) => {
            const order = comparator(a[0], b[0]);
            if (order !== 0) return order;
            return a[1] - b[1];
        });
        return stabilizedThis.map((el) => el[0]);
    }

    const headCells = [
        { id: 'title', numeric: false, disablePadding: false, label: 'Judul Film' },
        { id: 'year', numeric: true, disablePadding: false, label: 'Tahun' },
        { id: 'duration', numeric: true, disablePadding: false, label: 'Durasi (menit)' },
        { id: 'genre', numeric: false, disablePadding: false, label: 'Genre' },
        { id: 'rating', numeric: true, disablePadding: false, label: 'Rating' }
    ];

    function EnhancedTableHead(props) {
        const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
        const createSortHandler = (property) => (event) => {
            onRequestSort(event, property);
        };

        return (
            <TableHead>
                <TableRow>
                    <TableCell align="center">No.</TableCell>

                    {headCells.map((headCell) => (
                        <TableCell
                            key={headCell.id}
                            align={headCell.numeric ? 'right' : 'left'}
                            padding={headCell.disablePadding ? 'none' : 'default'}
                            sortDirection={orderBy === headCell.id ? order : false}
                        >
                            <TableSortLabel
                                active={orderBy === headCell.id}
                                direction={orderBy === headCell.id ? order : 'asc'}
                                onClick={createSortHandler(headCell.id)}
                            >
                                {headCell.label}
                                {orderBy === headCell.id ? (
                                    <span className={classes.visuallyHidden}>
                                        {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                    </span>
                                ) : null}
                            </TableSortLabel>
                        </TableCell>

                    ))}
                    <TableCell align="left">Aksi</TableCell>
                </TableRow>
            </TableHead>
        );
    }

    EnhancedTableHead.propTypes = {
        classes: PropTypes.object.isRequired,
        numSelected: PropTypes.number.isRequired,
        onRequestSort: PropTypes.func.isRequired,
        onSelectAllClick: PropTypes.func.isRequired,
        order: PropTypes.oneOf(['asc', 'desc']).isRequired,
        orderBy: PropTypes.string.isRequired,
        rowCount: PropTypes.number.isRequired,
    };

    //sampe sini

    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const handleSelectAllClick = (event) => {
        if (event.target.checked) {
            const newSelecteds = rows.map((n) => n.name);
            setSelected(newSelecteds);
            return;
        }
        setSelected([]);
    };

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    // if(rows) {
    //     const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
    // }else{
    //     const emptyRows = null
    // }
    const handleEdit = (event) => {
        let idMovie = parseInt(event.currentTarget.value)
        let movie = listMovie.find(x => x.id === idMovie)
        setinputMovie({
            title: movie.title,
            description: movie.description,
            year: movie.year,
            duration: movie.duration,
            genre: movie.genre,
            rating: movie.rating,
            created_at:movie.created_at,
            updated_at:movie.updated_at,
            image_url:movie.image_url
        })
        setselectedID(idMovie)
        history.push("/movie/edit");
    }

    const handleDelete = (event) => {
        let idMovie = parseInt(event.currentTarget.value)
        let newListMovie = listMovie.filter(el => el.id !== idMovie)

        axios.delete(`http://backendexample.sanbercloud.com/api/movies/${idMovie}`)
            .then(res => {
                console.log(res)
            })

        setlistMovie([...newListMovie])
    }

    return (
        <div className={classes.root}>
            {rows !== null &&
                <Paper className={classes.paper}>
                    
                    <TableContainer>
                        <Table
                            className={classes.table}
                            aria-labelledby="tableTitle"
                            aria-label="enhanced table"
                        >
                            <EnhancedTableHead
                                classes={classes}
                                numSelected={selected.length}
                                order={order}
                                orderBy={orderBy}
                                onSelectAllClick={handleSelectAllClick}
                                onRequestSort={handleRequestSort}
                                rowCount={rows.length}
                            />
                            <TableBody>
                                {stableSort(rows, getComparator(order, orderBy))
                                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    .map((movieObj, index) => {
                                        return (
                                            <TableRow
                                                hover
                                               
                                                tabIndex={-1}
                                                key={movieObj.name}
                                            >
                                                <TableCell>{index + 1}</TableCell>
                                                <TableCell>
                                                <Link to={`/movie/${movieObj.id}`}>{movieObj.title}</Link>
                                                </TableCell>
                                                <TableCell align="right">{movieObj.year}</TableCell>
                                                <TableCell align="right">{movieObj.duration}</TableCell>
                                                <TableCell align="left">{movieObj.genre}</TableCell>
                                                <TableCell align="right">{parseFloat(movieObj.rating).toFixed(1)}</TableCell>
                                                <TableCell align="left">
                                                <Button onClick={handleEdit} size="small" variant="contained" color="primary" value={movieObj.id} style={{ margin: theme.spacing(0.5) }}>Edit</Button>
                                                <Button onClick={handleDelete} size="small" variant="contained" color="secondary" value={movieObj.id} style={{ margin: theme.spacing(0.5) }}>Delete</Button></TableCell>
                                            </TableRow>
                                        );
                                    })}
                                {/* {emptyRows > 0 && (
                                    <TableRow>
                                        <TableCell colSpan={6} />
                                    </TableRow>
                                )} */}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <TablePagination
                        rowsPerPageOptions={[5, 10, 25]}
                        component="div"
                        count={rows.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onChangePage={handleChangePage}
                        onChangeRowsPerPage={handleChangeRowsPerPage}
                    />
                </Paper>
            }
        </div>
    );
}