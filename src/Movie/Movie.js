import React from "react"
import MovieList from "./MovieList"
import MovieFilter from "./MovieFilter"

const Movie = () => {

    return (
        <>
            <MovieFilter />
            <br></br>
            <MovieList />
        </>
    )
}

export default Movie