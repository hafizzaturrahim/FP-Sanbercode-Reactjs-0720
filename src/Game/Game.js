import React from "react"
import GameList from "./GameList"
import GameFilter from "./GameFilter"

const Game = () => {

    return (
        <>
            <GameFilter />
            <br></br>
            <GameList />
        </>
    )
}

export default Game