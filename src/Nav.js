import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { AuthContext } from "./Auth/AuthContext";
import { AppBar, Toolbar, Button } from '@material-ui/core';

import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const Nav = () => {
  const [authData, setauthData] = useContext(AuthContext)
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <AppBar position="fixed"
      title={<img id="logo" alt="" src={require('./public/img/logo2.png')} style={{ width: "150px", marginRight: "20px" }} />}>
      <Toolbar>
        <img id="logo" alt="" src={require('./public/img/logo2.png')} style={{ width: "150px", marginRight: "20px" }} />

        <Button color="inherit"><Link to="/" style={{ color: "white" }}>Home</Link></Button>
        <Button color="inherit"><Link to="/about" style={{ color: "white" }}>About</Link></Button>
        {authData.isLogin === true &&
          <>
            <Button color="inherit"><Link to="/game" style={{ color: "white" }}>Games</Link></Button>
            <Button color="inherit"><Link to="/movie" style={{ color: "white" }}>Movies</Link></Button>
           
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} style={{ color: "white" }}>
              Profil
            </Button>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              <MenuItem onClick={handleClose}><Link to="/profil" style={{ color: "black" }}>Ubah Password</Link></MenuItem>
              <MenuItem onClick={handleClose}><Link to="/logout" style={{ color: "black" }}>Logout</Link></MenuItem>
            </Menu>
          </>
        }
        {authData.isLogin === false &&
          <>
            <Button color="inherit"><Link to="/login" style={{ color: "white" }}>Login</Link></Button>
          </>
        }
      </Toolbar>
    </AppBar>
  )
}

export default Nav