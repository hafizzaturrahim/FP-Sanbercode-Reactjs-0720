import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import Routes from './Routes';
import { AuthProvider } from "./Auth/AuthContext";
import './public/css/style.css'


function App() {
  return (
    <div>
    {document.title = "Sanbergamov"}
      <AuthProvider>
        <Router>
          <Routes />
        </Router>
      </AuthProvider>
    </div>

  );
}

export default App;
