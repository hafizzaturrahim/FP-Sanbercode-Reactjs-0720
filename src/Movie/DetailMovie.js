import React, { Component } from "react";
import axios from 'axios';
import { Grid } from "@material-ui/core";

class DetailMovie extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: this.props.match.params.id,
            movie: null
        }

        console.log(this.state.id)
    }

    componentDidMount() {
        axios.get(`https://backendexample.sanbersy.com/api/movies/${this.state.id}`)
            .then(res => {
                console.log("detail movie")
                console.log(res)
                let movie = {

                    title: res.data.title,
                    description: res.data.description,
                    rating: res.data.rating,
                    duration: res.data.duration,
                    genre: res.data.genre,
                    created_at: res.data.created_at,
                    updated_at: res.data.updated_at,
                    image_url: res.data.image_url,
                    review:res.data.review
                }
                this.setState({ movie })
            })
    }

    render() {
        return (
            <>
                {
                    this.state.movie !== null &&
                    <div>
                        <h2>{this.state.movie.title}</h2>
                        <Grid container spacing={2}>
                            <Grid item sm={3} xs={12}>

                            <img src={this.state.movie.image_url} style={{ width: "180px", height:"240px",border: "5px solid #555" }} onError={(e)=>{e.target.onerror = null; e.target.src="../public/img/no_image.jpg"}}/>

                            </Grid>
                            <Grid item sm={9} xs={12}>
                                <h3>Rating : {parseFloat(this.state.movie.rating).toFixed(1)} </h3>
                                <p><strong>Durasi :</strong> {this.state.movie.duration} menit</p>
                                <p><strong>Genre :</strong> {this.state.movie.genre} </p>
                                <p><strong>Deskripsi : </strong> <br></br> {this.state.movie.description}
                                </p>
                                <p><strong>Review : </strong> <br></br> {this.state.movie.review}
                                </p>
                               
                            </Grid>
                        </Grid>
                    </div>
                }
            </>
        )
    }
}

export default DetailMovie
