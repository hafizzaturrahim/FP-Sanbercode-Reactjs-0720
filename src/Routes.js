import React, { useContext } from "react";
import { Switch, Link, Route, useHistory, Redirect } from "react-router-dom";

import Nav from "./Nav";
import About from "./about";
import Movie from "./Movie/Movie";
import MovieHome from "./Movie/MovieHome";
import MovieForm from "./Movie/MovieForm";
import DetailMovie from "./Movie/DetailMovie";
import { MovieProvider } from "./Movie/MovieContext";
import { InputProvider } from "./Movie/InputContext";

import GameRoute from "./Game/GameRoute";

import Game from "./Game/Game";
import GameForm from "./Game/GameForm";
import DetailGame from "./Game/DetailGame";
import { GameProvider } from "./Game/GameContext";

import Login from "./Auth/Login";
import Register from "./Auth/Register";
import { AuthContext } from "./Auth/AuthContext";

import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import ChangePassword from "./Auth/ChangePassword";


const Routes = () => {
  const [authData, setauthData] = useContext(AuthContext)
  const history = useHistory();

  const handleLogout = () => {
    setauthData({ isLogin: false, id: "" })
    localStorage.removeItem('user')
    history.push("/login");
  }

  const LoginRoute = ({ user, ...props }) =>
    localStorage.getItem('user') ? <Redirect to="/" /> : <Route {...props} />;

  // const PrivateRoute = ({ user, ...props }) =>
  //   localStorage.getItem('user') ? <Route {...props} /> : <Redirect to="/login" />;
  const PrivateRoute = ({ component: Component, users, ...rest }) => {
    return (
      <Route
        {...rest}
        render={(props) => localStorage.getItem('user') ?
          <Component {...props} />
          : <Redirect to={{ pathname: "/login", state: { from: props.location.pathname } }} />
        }
      />
    )
  };

  return (
    <>
      <Nav />

      <CssBaseline />
      <Container maxWidth="md" style={{ backgroundColor: "white", marginTop: "96px", padding: "24px", marginBottom: "64px" }}>

        <Switch>
          <Route exact path="/" component={MovieHome} />

          <Route exact path="/about">
            <About />
          </Route>

          <Route exact path="/register" component={Register} />
          <LoginRoute exact path="/login" user={authData} component={Login} />
          <Route exact path="/logout" user={authData}>
            {handleLogout}
          </Route>

          <PrivateRoute exact path="/profil" user={authData} component={ChangePassword} />
          <PrivateRoute exact path="/game" component={() => <GameRoute param={1} />} />
          <PrivateRoute exact path="/game/create" component={() => <GameRoute param={2} />} />
          <PrivateRoute exact path="/game/:id" component={() => <GameRoute param={3} />} />
          <PrivateRoute exact path="/game/edit/:id" component={() => <GameRoute param={4} />} />

          {/* <GameProvider>
            <PrivateRoute exact path="/game" component={Game} />
            <PrivateRoute exact path="/game/create" component = {GameForm} />
            <PrivateRoute exact path="/game/:id" component={DetailGame} />
            <PrivateRoute exact path="/game/edit/:id" user={authData} exact component={GameForm} />
          </GameProvider> */}

          <MovieProvider>
            <PrivateRoute exact path="/movie/:id" user={authData} component={DetailMovie} />
            <InputProvider>
              <PrivateRoute exact path="/movie" component={Movie} />
              <PrivateRoute exact path="/movie/create" component={MovieForm} />
              <PrivateRoute exact path="/movie/edit" component={MovieForm} />
            </InputProvider>
          </MovieProvider>

        </Switch>
      </Container>
      <footer>
        <h5>copyright &copy; 2020 by Sanbercode</h5>
      </footer>
    </>
  );
};

export default Routes;
