import React, { useState, useEffect, createContext } from "react";
import axios from 'axios';

export const GameContext = createContext();
export const GameProvider = props => {
    const [listGame, setListGame] = useState(null)
    
    useEffect(() => {
        if (listGame === null) {
            axios.get(`https://backendexample.sanbersy.com/api/games`)
                    .then(res => {
                        setListGame(res.data.map(el => {
                            return {
                                id: el.id,
                                created_at:el.created_at,
                                updated_at:el.updated_at,
                                name: el.name,
                                genre: el.genre,
                                singlePlayer: el.singlePlayer,
                                multiplayer: el.multiplayer,
                                platform: el.platform,
                                release:el.release
                            }
                        }))
                        
                    })
        }

    }, [listGame])
    
    return (
        <GameContext.Provider value={[listGame, setListGame]}>
            {props.children}
        </GameContext.Provider>
    );
};