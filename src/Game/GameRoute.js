import React from "react";
import { useParams } from "react-router-dom";

import Game from "./Game";
import GameForm from "./GameForm";
import DetailGame from "./DetailGame";
import { GameProvider } from "./GameContext";

const GameRoute = ({ param }) => {
    let { id } = useParams()

    return (
        <>
            <GameProvider>
                {param === 1 &&
                    <Game />
                }
                {param === 2 &&
                    <GameForm />
                }
                {param === 3 &&
                    <DetailGame id={id}/>
                }
                {param === 4 &&
                    <GameForm idParam={id}/>
                }
            </GameProvider>
        </>
    )
}

export default GameRoute