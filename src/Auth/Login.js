import React, { useState, useContext } from 'react';
import { AuthContext } from "./AuthContext";
import { Link, useHistory } from "react-router-dom";
import axios from 'axios';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(4),
        marginBottom: theme.spacing(4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function Login() {
    const classes = useStyles();
    const [authData, setauthData] = useContext(AuthContext)

    const [notif, setnotif] = useState("")
    const history = useHistory();
    console.log(localStorage.getItem('user'))

    const handleLogin = (event) => {
        event.preventDefault();
        let username = event.target.username.value
        let password = event.target.password.value
        
        axios.post(`https://backendexample.sanbersy.com/api/login`, { username, password })
        .then(res => {
            console.log("sukses")
            console.log(res.data)
            
            if(res.data !== "invalid username or password"){
                setauthData({ isLogin: true, id: res.data.id })
                console.log("masuk")

                //session
                localStorage.setItem('user', res.data.id);

                history.push("/");
            }else{
                setnotif("username atau password salah")
            }
        })
        .catch(res => {
            console.log("ada error")
            console.log(res)
        })

    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign in
                </Typography>

                <form className={classes.form} noValidate onSubmit={handleLogin}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="username"
                        label="Username"
                        name="username"
                        
                        
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Login
                    </Button>
                    { notif !== null && 
                        <p style={{color:"red"}}>{notif} </p>
                    
                    }
                    <Grid container>
                        
                        <Grid item>
                        <p>Belum punya akun? Daftar di <Link to="/register">sini</Link></p>
                        </Grid>
                    </Grid>
                </form>
            </div>
        </Container>
    );
}