import React, { useContext, useEffect } from "react"
import { GameContext } from "./GameContext";
import axios from 'axios';
import { Link, useHistory } from "react-router-dom";

import PropTypes from 'prop-types';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
    },
    table: {
        minWidth: 750,
    },
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
}));

export default function GameList() {
    const classes = useStyles();
    const [order, setOrder] = React.useState('asc');
    const [orderBy, setOrderBy] = React.useState('calories');
    const [selected, setSelected] = React.useState([]);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);

    const [listGame, setListGame] = useContext(GameContext)

    const theme = useTheme();

    useEffect(() => {
        if (listGame === null) {
            axios.get(`https://backendexample.sanbersy.com/api/games`)
                .then(res => {
                    setListGame(res.data.map(el => {
                        return {
                            id: el.id,
                            created_at: el.created_at,
                            updated_at: el.updated_at,
                            name: el.name,
                            genre: el.genre,
                            singlePlayer: el.singlePlayer,
                            multiplayer: el.multiplayer,
                            platform: el.platform,
                            release: el.release
                        }
                    }))

                })
        }

    }, [listGame])
    // dari atas
    const rows = listGame;
    // console.log("rows")
    // console.log(listMovie)
    function descendingComparator(a, b, orderBy) {
        if (b[orderBy] < a[orderBy]) {
            return -1;
        }
        if (b[orderBy] > a[orderBy]) {
            return 1;
        }
        return 0;
    }

    function getComparator(order, orderBy) {
        return order === 'desc'
            ? (a, b) => descendingComparator(a, b, orderBy)
            : (a, b) => -descendingComparator(a, b, orderBy);
    }

    function stableSort(array, comparator) {
        const stabilizedThis = array.map((el, index) => [el, index]);
        stabilizedThis.sort((a, b) => {
            const order = comparator(a[0], b[0]);
            if (order !== 0) return order;
            return a[1] - b[1];
        });
        return stabilizedThis.map((el) => el[0]);
    }

    const headCells = [
        { id: 'name', numeric: false, disablePadding: false, label: 'Judul Game' },
        { id: 'genre', numeric: false, disablePadding: false, label: 'Genre' },
        { id: 'singlePlayer', numeric: false, disablePadding: false, label: 'Single Player?' },
        { id: 'multiplayer', numeric: false, disablePadding: false, label: 'Multi Player?' },
        { id: 'platform', numeric: false, disablePadding: false, label: 'Platform' },
        { id: 'release', numeric: true, disablePadding: false, label: 'Tahun Rilis' },
    ];

    function EnhancedTableHead(props) {
        const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
        const createSortHandler = (property) => (event) => {
            onRequestSort(event, property);
        };

        return (
            <TableHead>
                <TableRow>
                    <TableCell align="center">No.</TableCell>

                    {headCells.map((headCell) => (
                        <TableCell
                            key={headCell.id}
                            align={headCell.numeric ? 'right' : 'left'}
                            padding={headCell.disablePadding ? 'none' : 'default'}
                            sortDirection={orderBy === headCell.id ? order : false}
                        >
                            <TableSortLabel
                                active={orderBy === headCell.id}
                                direction={orderBy === headCell.id ? order : 'asc'}
                                onClick={createSortHandler(headCell.id)}
                            >
                                {headCell.label}
                                {orderBy === headCell.id ? (
                                    <span className={classes.visuallyHidden}>
                                        {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                    </span>
                                ) : null}
                            </TableSortLabel>
                        </TableCell>

                    ))}
                    <TableCell align="left">Aksi</TableCell>
                </TableRow>
            </TableHead>
        );
    }

    EnhancedTableHead.propTypes = {
        classes: PropTypes.object.isRequired,
        numSelected: PropTypes.number.isRequired,
        onRequestSort: PropTypes.func.isRequired,
        onSelectAllClick: PropTypes.func.isRequired,
        order: PropTypes.oneOf(['asc', 'desc']).isRequired,
        orderBy: PropTypes.string.isRequired,
        rowCount: PropTypes.number.isRequired,
    };

    //sampe sini

    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const handleSelectAllClick = (event) => {
        if (event.target.checked) {
            const newSelecteds = rows.map((n) => n.name);
            setSelected(newSelecteds);
            return;
        }
        setSelected([]);
    };

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    // if(rows) {
    //     const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
    // }else{
    //     const emptyRows = null
    // }
    const handleDelete = (event) => {
        let idGame = parseInt(event.currentTarget.value)
        let newListGame = listGame.filter(el => el.id !== idGame)

        axios.delete(`https://backendexample.sanbersy.com/api/games/${idGame}`)
            .then(res => {
                console.log(res)
            })

        setListGame([...newListGame])
    }

    return (
        <div className={classes.root}>
            {rows !== null &&
                <Paper className={classes.paper}>

                    <TableContainer>
                        <Table
                            className={classes.table}
                            aria-labelledby="tableTitle"
                            aria-label="enhanced table"
                        >
                            <EnhancedTableHead
                                classes={classes}
                                numSelected={selected.length}
                                order={order}
                                orderBy={orderBy}
                                onSelectAllClick={handleSelectAllClick}
                                onRequestSort={handleRequestSort}
                                rowCount={rows.length}
                            />
                            <TableBody>
                                {stableSort(rows, getComparator(order, orderBy))
                                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    .map((gameObj, index) => {
                                        return (
                                            <TableRow
                                                hover
                                                tabIndex={-1}
                                                key={gameObj.name}
                                            >
                                                <TableCell>{index + 1}</TableCell>
                                                <TableCell>
                                                    <Link to={`/game/${gameObj.id}`}>{gameObj.name}</Link>
                                                </TableCell>
                                                <TableCell align="left">{gameObj.genre}</TableCell>
                                                <TableCell align="left">{gameObj.singlePlayer === 1 ? `Ya` : `Tidak`}</TableCell>
                                                <TableCell align="left">{gameObj.multiplayer === 1 ? `Ya` : `Tidak`}</TableCell>
                                                <TableCell align="left">{gameObj.platform}</TableCell>
                                                <TableCell align="right">{gameObj.release}</TableCell>
                                                <TableCell align="left">
                                                    <Button color="primary" size="small" variant="contained" style={{ margin: "4px" }}><Link to={`/game/edit/${gameObj.id}`} style={{ color: "white" }}>Edit</Link></Button>
                                                    <Button onClick={handleDelete} size="small" variant="contained" color="secondary" value={gameObj.id}>Delete</Button>
                                                </TableCell>
                                            </TableRow>
                                        );
                                    })}
                                {/* {emptyRows > 0 && (
                                    <TableRow>
                                        <TableCell colSpan={6} />
                                    </TableRow>
                                )} */}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <TablePagination
                        rowsPerPageOptions={[5, 10, 25]}
                        component="div"
                        count={rows.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onChangePage={handleChangePage}
                        onChangeRowsPerPage={handleChangeRowsPerPage}
                    />
                </Paper>
            }
        </div>
    );
}