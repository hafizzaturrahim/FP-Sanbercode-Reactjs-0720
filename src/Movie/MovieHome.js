import React, { useContext, useState, useEffect } from 'react';
import { MovieContext } from './MovieContext';
import { Link } from "react-router-dom";
import axios from 'axios';

const MovieHome = () => {
    const [listMovie, setlistMovie] = useState(null)
    
    console.log(localStorage.getItem('user'))
   
    let listMoviebyRating = null

    useEffect(() => {
        if (listMovie === null) {
            axios.get(`https://backendexample.sanbersy.com/api/movies`)
                .then(res => {
                    console.log("read data")
                    setlistMovie(res.data.map(el => {
                        return {
                            id: el.id,
                            title: el.title,
                            description: el.description,
                            year: el.year,
                            duration: el.duration,
                            genre: el.genre,
                            rating: el.rating
                        }
                    }))

                })
        }

    }, [listMovie])

    if(listMovie !== null)  {   
        listMoviebyRating= listMovie.sort(function(a, b){
            return b.rating-a.rating
        })
        
    }

    return (
        <div>
            <h1>Daftar Film - Film Terbaik</h1>
            <div id="article-list">
                {listMovie !== null && listMoviebyRating !==null && listMoviebyRating.map((movieObj, index) => {
                        return (
                            <article>
                                <a><h3><Link to={`/movie/${movieObj.id}`}>{movieObj.title}</Link></h3></a>
                                <h4>Rating {parseFloat(movieObj.rating).toFixed(1)} </h4>
                                <h4>Durasi {movieObj.duration} </h4>
                                <h4>Genre : {movieObj.genre} </h4>
                                <p><Link to={`/movie/${movieObj.id}`}>Read more ...</Link>
                                </p>
                            </article>
                        )
                    })
                }
            </div>
        </div>
    )
}

export default MovieHome